﻿using System;

namespace DrawSquare
{
    class Program
    {
        static void Main(string[] args)
        {
            bool correctInt = false;
            Console.WriteLine("Write an integer larger than zero");
            int num1 = 0;
            while (!correctInt)
            {
                try
                {
                    num1 = int.Parse(Console.ReadLine());
                    if(num1 > 0)
                        correctInt = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.WriteLine("Try again");
                }
            }
             
            for (int i = 0; i < num1 + 1; i++)
            {
                for(int j = 0; j < num1 + 1; j++)
                {
                    if (i == 0 || i == num1 || (num1 >= 5 && ((i == 2 && (j != 1 && j != num1-1)) || (i == num1 - 2 && (j != num1 - 1 && j != 1)))))
                        Console.Write("*");
                    else if (j == 0 || j == num1 || (num1 >= 5 && ((j == 2 && (i != 1 && i != num1 - 1)) || (j == num1 - 2 && (i != num1 - 1 && i != 1)))))
                        Console.Write("*");         
                    else
                        Console.Write(" ");
                }
                Console.WriteLine();
            }
        }
    }
}
